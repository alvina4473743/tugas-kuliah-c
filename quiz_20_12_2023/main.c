// main.c
#include <stdio.h>
#include <stdlib.h>
#include "gaji.h"

int main() {
    system("clear");

    char nama[50];
    char gol = ' ';
    char status[10];

    float gapok = 0.0;
    float gaber = 0.0;
    float tunja = 0.0;
    float pot = 0.0;
    float prosen_pot = 0.05;

    jdl_aplikasi();
    input(nama, &gol, status);
    gapok_tunja(gol, status, &gapok, &tunja);

    // Rest of the main function...

    output(gapok, tunja, pot, gaber);

    return 0;
}
