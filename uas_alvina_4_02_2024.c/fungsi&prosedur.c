#include <stdio.h>

// Berfungsi untuk menghitung huruf nilai berdasarkan skor
char calculateGrade(double score) {
    if (score > 85 && score <= 100)
        return 'A';
    else if (score > 70 && score <= 85)
        return 'B';
    else if (score > 55 && score <= 70)
        return 'C';
    else if (score > 40 && score <= 55)
        return 'D';
    else if (score >= 0 && score <= 40)
        return 'E';
    return 'F'; // Nilai default jika tidak ada kondisi yang terpenuhi
}

// Prosedur untuk memproses skor dan menghitung nilai
void processScores() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;
    quiz = 40; absen = 100; uts = 60; uas = 50; tugas = 80;
    printf("Absen = %.2f UTS = %.2f\n", absen, uts);
    printf("Tugas = %.2f UAS = %.2f\n", tugas, uas);
    printf("Quiz = %.2f\n", quiz);
    nilai = ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;
    Huruf_Mutu = calculateGrade(nilai);
    printf("Huruf Mutu : %c\n", Huruf_Mutu);
}

int main() {
    processScores();
    return 0;
}



Absen = 100.00 UTS = 60.00
Tugas = 80.00 UAS = 50.00
Quiz = 40.00
Huruf Mutu : D
