#include <stdio.h>

// Berfungsi untuk menghitung nilai rata-rata
double calculateWeightedAverage(double absen, double tugas, double quiz, double uts, double uas) {
    return ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) /  2;
}

// Berfungsi untuk menentukan nilai huruf
char determineLetterGrade(double nilai) {
    char Huruf_Mutu;
    if (nilai >  85 && nilai <=  100)
        Huruf_Mutu = 'A';
    else if (nilai >  70 && nilai <=  85)
        Huruf_Mutu = 'B';
    else if (nilai >  55 && nilai <=  70)
        Huruf_Mutu = 'C';
    else if (nilai >  40 && nilai <=  55)
        Huruf_Mutu = 'D';
    else if (nilai >=  0 && nilai <=  40)
        Huruf_Mutu = 'E';
    
    return Huruf_Mutu;
}

int main() {
    // Tentukan jumlah siswa
    int num_students =  2;
    // Perulangan untuk memproses setiap siswa
    for (int i =  0; i < num_students; i++) {
        double nilai, quiz, absen, uts, uas, tugas;
        char Huruf_Mutu;

        printf("Masukkan nilai untuk siswa %d:\n", i +  1);
        printf("Absen = ");
        scanf("%lf", &absen);
        printf("UTS = ");
        scanf("%lf", &uts);
        printf("Tugas = ");
        scanf("%lf", &tugas);
        printf("Quiz = ");
        scanf("%lf", &quiz);
        printf("UAS = ");
        scanf("%lf", &uas);

        nilai = calculateWeightedAverage(absen, tugas, quiz, uts, uas);
        Huruf_Mutu = determineLetterGrade(nilai);

        printf("\nMahasiswa %d Nilai:\n", i +  1);
        printf("Absen = %.2f UTS = %.2f\n", absen, uts);
        printf("Tugas = %.2f UAS = %.2f\n", tugas, uas);
        printf("Quiz = %.2f\n", quiz);
        printf("Nilai Rata-rata = %.2f\n", nilai);
        printf("Huruf Mutu : %c\n", Huruf_Mutu);
    }

    return  0;
}


Masukkan nilai untuk siswa 1:
Absen = 100
UTS = 50
Tugas = 80
Quiz = 100
UAS = 90

Mahasiswa 1 Nilai:
Absen = 100.00 UTS = 50.00
Tugas = 80.00 UAS = 90.00
Quiz = 100.00
Nilai Rata-rata = 60.50
Huruf Mutu : C
Masukkan nilai untuk siswa 2:
Absen = 100
UTS = 90
Tugas = 100
Quiz = 100
UAS = 90

Mahasiswa 2 Nilai:
Absen = 100.00 UTS = 90.00
Tugas = 100.00 UAS = 90.00
Quiz = 100.00
Nilai Rata-rata = 70.50
Huruf Mutu : B


...Program finished with exit code 0
Press ENTER to exit console.
